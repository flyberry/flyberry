use strict;
use warnings;
use HiPi::Interface::MPL3115A2;
use HiPi::BCM2835;
use HiPi::Utils;
use IO::Socket::INET;

sub init_sensor() {
	HiPi::BCM2835->bcm2835_init();
}

sub getLoggingTime() {
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
	my $nice_timestamp = sprintf ( "%04d-%02d-%02d %02d:%02d:%02d", $year+1900,$mon+1,$mday,$hour,$min,$sec);
	return $nice_timestamp;
}

sub getRandomFilename() {
	my $rn_1 = rand(9999);
	my $rn_2 = rand(8888);
	my $rn_3 = rand(7777);
	my $rn_4 = rand(6666);
	return $rn_1.$rn_2.$rn_3.$rn_4;
}

sub sendFile {
	my ($filename, $socket) = @_;
	open FILE, $filename;
	while (<FILE>) {
		print $socket $_;
	}
	close FILE;
	$socket->send("END");
}

my $socket = new IO::Socket::INET(
	PeerHost => '172.20.174.174',
	PeerPort => '975',
	Proto => 'tcp'
);
die "cannot connect to the server $!\n" unless $socket;
print "connected to the server\n";


for (;;) {
	my $random = "images/".getRandomFilename().".jpg";
	system("raspistill -w 640 -h 480 -o ".$random);
	sleep 3;
	init_sensor();
	my $dev = HiPi::Interface::MPL3115A2->new;
	my ($alt, $pre, $tem) = $dev->os_all_data();
	my $req = $tem.';'.$pre.';'.$alt;
	my $size = $socket->send("MESSDATEN;".$req);
	open(my $fh, '>>', 'report.txt');
	print $fh "$req" . ';' . getLoggingTime() . "\n";
	close $fh;
	sendFile($random, $socket);
}
