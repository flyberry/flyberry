﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using MySql.Data.MySqlClient;

namespace Strato_Terminal
{
    public partial class Form1 : Form
    {
        private string connectionstring = "Server=127.0.0.1;Database=skyberry;Uid=root;Pwd=;";
        private string dir = "C:\\xampp\\htdocs\\skyberry\\images\\";
        private delegate void d_changeVisuals(Boolean connection_stable);
        private delegate void d_write(string txt);
        private TcpListener tcpListener;
        private Thread listenThread;

        private void write(string txt)
        {
            richTextBox1.AppendText(txt + "\n");
        }

        private void changeVisuals(Boolean connection_stable) {
            if (connection_stable)
            {
                toolStripStatusLabel1.Image = Image.FromFile(Application.StartupPath + "//images/bullet_green.png");
                label1.Text = "Verbindung besteht";
                label1.ForeColor = Color.LimeGreen;
            }
            else
            {
                toolStripStatusLabel1.Image = Image.FromFile(Application.StartupPath + "//images/bullet_red.png");
                label1.Text = "Keine Verbindung";
                label1.ForeColor = Color.Red;
            }
            
        }

        public Form1()
        {
            InitializeComponent();
            this.tcpListener = new TcpListener(IPAddress.Any, 975);
            this.listenThread = new Thread(new ThreadStart(listenForClients));
            this.listenThread.Start();
        }

        private void listenForClients()
        {
            this.tcpListener.Start();

            while (true)
            {
                TcpClient client = this.tcpListener.AcceptTcpClient();
                d_changeVisuals handler = changeVisuals;
                handler(true);
                Thread clientThread = new Thread(new ParameterizedThreadStart(handleClientCommunication));
                clientThread.Start(client);
            }
        }

        private void handleClientCommunication(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();
            byte[] message = new byte[3000000];
            int bytesRead;

            while (true)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = clientStream.Read(message, 0, message.Length);
                }
                catch {
                    // error
                    break;
                }

                if (bytesRead == 0)
                {
                    d_changeVisuals handler = changeVisuals;
                    handler(false);
                    break;
                }

                Random rnd = new Random();
                string newfilename = DateTime.Now.Millisecond + rnd.Next(99999, 99999) + ".jpg";

                ASCIIEncoding encoder = new ASCIIEncoding();
                string clientMessage = encoder.GetString(message, 0, bytesRead);
                if (clientMessage.Contains("MESSDATEN;"))
                {
                    d_write handler = write;
                    handler("[" + DateTime.Now.ToShortTimeString() + "] Messdaten empfangen");
                    string[] parts = clientMessage.Split(new char[] { ';' });
                    save_measure(parts[1], parts[2], parts[3]);
                }
                else {
                    if (clientMessage.Contains("END"))
                    {
                        d_write handler = write;
                        handler("[" + DateTime.Now.ToShortTimeString() + "] Bild erfolgreich empfangen");
                        if (File.Exists(dir + "live_image.jpg"))
                        {
                            File.Delete(dir + "live_image.jpg");
                        }

                        File.Move(dir + "temp", dir + "live_image.jpg");
                    }
                    else
                    {
                        d_write handler = write;
                        handler("[" + DateTime.Now.ToShortTimeString() + "] Bild wird empfangen...");
                        FileStream Fs = new FileStream(dir + "temp", FileMode.Append, FileAccess.Write);
                        Fs.Write(message, 0, bytesRead);
                        Fs.Close();
                    }
                }
              
            }

            tcpClient.Close();
        }

        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void save_measure(string temp, string druck, string hoehe)
        {
            MySqlConnection conn = new MySqlConnection(this.connectionstring);
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO messungen (temperatur, luftdruck, hoehe) VALUES ('" + temp + "', '" + druck + "', '" + hoehe + "')";
            conn.Open();
            cmd.ExecuteNonQuery();
        }
    }
}
