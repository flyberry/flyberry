<?php
	$database = new mysqli("localhost", "root", "", "skyberry");
	$result = $database->query("(SELECT * FROM messungen ORDER BY id DESC LIMIT 10) ORDER by id ASC");
	$temp_collection = null;
	$time_collection = null;
	$height_collection = null;
	while ($row = $result->fetch_array()) {
		$temp_collection[] = round(floatval($row['temperatur']), 1);
		$time_collection[] = explode(" ", $row['zeitstempel'])[1];
		$height_collection[] = round(floatval($row['hoehe']), 1);
		$air_pressure_collection[] = round(floatval($row['luftdruck']), 1);
	}
	
	echo json_encode($temp_collection) . ";" . json_encode($time_collection) . ";" . json_encode($height_collection) . ";" . json_encode($air_pressure_collection);
?>