use warnings;
use HiPi::Interface::MPL3115A2;
use HiPi::BCM2835;
use HiPi::Utils;

if ($<) {die 'script must be run using sudo'; }
my $username = getpwuid($<);
print qq(Start up user $username \n);
my $targetuser = getlogin() || 'pi';
HiPi::BCM2835->bcm2835_init();

my $dev = HiPi::Interface::MPL3115A2->new;
my $devid = $dev->who_am_i;
print qq(Device ID $devid\n);
my ($alt, $pre, $tem) = $dev->os_all_data();
print qq(Temperature $tem\n);
